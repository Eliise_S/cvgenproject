﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CvGenProject.Startup))]
namespace CvGenProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
