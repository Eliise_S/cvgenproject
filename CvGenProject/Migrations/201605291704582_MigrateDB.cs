namespace CvGenProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrateDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TemplateForms",
                c => new
                    {
                        TemplateFormId = c.Int(nullable: false, identity: true),
                        TemplateId = c.Int(nullable: false),
                        UserId = c.String(),
                        TemplateName = c.String(),
                        Name = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Profile = c.String(),
                        Skill1Name = c.String(),
                        Skill1Description = c.String(),
                        Skill2Name = c.String(),
                        Skill2Description = c.String(),
                        Skill3Name = c.String(),
                        Skill3Description = c.String(),
                        Skill4Name = c.String(),
                        Skill4Description = c.String(),
                        Skill5Name = c.String(),
                        Skill5Description = c.String(),
                        Skill6Name = c.String(),
                        Skill6Description = c.String(),
                        Technical = c.String(),
                        Experience1Name = c.String(),
                        Experience1Period = c.String(),
                        Experience1Position = c.String(),
                        Experience1Description = c.String(),
                        Experience2Name = c.String(),
                        Experience2Period = c.String(),
                        Experience2Position = c.String(),
                        Experience2Description = c.String(),
                        Experience3Name = c.String(),
                        Experience3Period = c.String(),
                        Experience3Position = c.String(),
                        Experience3Description = c.String(),
                        Experience4Name = c.String(),
                        Experience4Period = c.String(),
                        Experience4Position = c.String(),
                        Experience4Description = c.String(),
                        Experience5Name = c.String(),
                        Experience5Period = c.String(),
                        Experience5Position = c.String(),
                        Experience5Description = c.String(),
                        Experience6Name = c.String(),
                        Experience6Period = c.String(),
                        Experience6Position = c.String(),
                        Experience6Description = c.String(),
                        Education1School = c.String(),
                        Education1Name = c.String(),
                        Education1Period = c.String(),
                        Education2School = c.String(),
                        Education2Name = c.String(),
                        Educatio2Period = c.String(),
                        Education3School = c.String(),
                        Education3Name = c.String(),
                        Education3Period = c.String(),
                        Education4School = c.String(),
                        Education4Name = c.String(),
                        Education4Period = c.String(),
                        Education5School = c.String(),
                        Education5Name = c.String(),
                        Education5Period = c.String(),
                        Education6School = c.String(),
                        Education6Name = c.String(),
                        Education6Period = c.String(),
                    })
                .PrimaryKey(t => t.TemplateFormId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TemplateForms");
        }
    }
}
