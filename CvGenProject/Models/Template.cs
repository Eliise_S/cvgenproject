﻿
namespace CvGenProject.Models
{
    public class Template
    {
        public int TemplateId { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string PicturePreview { get; set; }
        public string PictureMain { get; set; }
        public string PreviewUrl { get; set; }
    }
}