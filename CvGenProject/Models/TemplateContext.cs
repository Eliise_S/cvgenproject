﻿using System.Data.Entity;

namespace CvGenProject.Models
{
    public class TemplateContext : DbContext
    {
        public DbSet<Template> Templates { get; set; }
        public DbSet<TemplateForm> TemplateForms { get; set; }
    }
}