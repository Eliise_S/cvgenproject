﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CvGenProject.Models
{
    public class TemplateForm
    {
        [HiddenInput(DisplayValue = false)]
        public int TemplateFormId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int TemplateId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CvUrl { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UserId { get; set; }

        [Display(Name = "Template name")]
        public string TemplateName { get; set; }

        [Display(Name = "Your full name")]
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        [Display(Name = "About you")]
        [DataType(DataType.MultilineText)]
        public string Profile { get; set; }

        // SKILLS SECTION
        [Display(Name = "Skill name")]
        public string Skill1Name { get; set; }

        [Display(Name = "Skill description")]
        [DataType(DataType.MultilineText)]
        public string Skill1Description { get; set; }

        [Display(Name = "Skill-2 name")]
        public string Skill2Name { get; set; }
    
        [Display(Name = "Skill-2 description")]
        [DataType(DataType.MultilineText)]
        public string Skill2Description { get; set; }

        [Display(Name = "Skill-3 name")]
        public string Skill3Name { get; set; }

        [Display(Name = "Skill-3 description")]
        [DataType(DataType.MultilineText)]
        public string Skill3Description{ get; set; }

        [Display(Name = "Skill-4 name")]
        public string Skill4Name { get; set; }

        [Display(Name = "Skill-4 description")]
        [DataType(DataType.MultilineText)]
        public string Skill4Description { get; set; }

        [Display(Name = "Skill-5 name")]
        public string Skill5Name { get; set; }

        [Display(Name = "Skill-5 description")]
        [DataType(DataType.MultilineText)]
        public string Skill5Description { get; set; }

        [Display(Name = "Skill-6 name")]
        public string Skill6Name { get; set; }

        [Display(Name = "Skill-6 description")]
        [DataType(DataType.MultilineText)]
        public string Skill6Description { get; set; }

        // EXPERIENCE SECTION
        [DataType(DataType.MultilineText)]
        public string Technical { get; set; }

        // EXPERIENCE SECTION
        //JOB 1
        [Display(Name = "Company name")]
        public string Experience1Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience1Period { get; set; }


        [Display(Name = "Position name")]
        public string Experience1Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience1Description { get; set; }

        //JOB 2
        [Display(Name = "Company-2 name")]
        public string Experience2Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience2Period { get; set; }

        [Display(Name = "Position name")]
        public string Experience2Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience2Description { get; set; }

        //JOB 3
        [Display(Name = "Company-3 name")]
        public string Experience3Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience3Period { get; set; }

        [Display(Name = "Position name")]
        public string Experience3Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience3Description { get; set; }

        //JOB 4
        [Display(Name = "Company-4 name")]
        public string Experience4Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience4Period { get; set; }

        [Display(Name = "Position name")]
        public string Experience4Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience4Description { get; set; }

        //JOB 5
        [Display(Name = "Company-5 name")]
        public string Experience5Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience5Period { get; set; }

        [Display(Name = "Position name")]
        public string Experience5Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience5Description { get; set; }

        //JOB 6
        [Display(Name = "Company-6 name")]
        public string Experience6Name { get; set; }

        [Display(Name = "Working period")]
        public string Experience6Period { get; set; }

        [Display(Name = "Position name")]
        public string Experience6Position { get; set; }

        [Display(Name = "Job description")]
        [DataType(DataType.MultilineText)]
        public string Experience6Description { get; set; }

        //EDUCATION SECTION
        //EDUCATION 1
        [Display(Name = "School name")]
        public string Education1School { get; set; }

        [Display(Name = "Education name")]
        public string Education1Name { get; set; }

        [Display(Name = "Education period")]
        public string Education1Period { get; set; }

        //EDUCATION 2
        [Display(Name = "School name")]
        public string Education2School { get; set; }

        [Display(Name = "Education-2 name")]
        public string Education2Name { get; set; }

        [Display(Name = "Education period")]
        public string Education2Period { get; set; }

        //EDUCATION 3
        [Display(Name = "School name")]
        public string Education3School { get; set; }

        [Display(Name = "Education-3 name")]
        public string Education3Name { get; set; }

        [Display(Name = "Education period")]
        public string Education3Period { get; set; }

        //EDUCATION 4
        [Display(Name = "School name")]
        public string Education4School { get; set; }

        [Display(Name = "Education-4 name")]
        public string Education4Name { get; set; }

        [Display(Name = "Education period")]
        public string Education4Period { get; set; }

        //EDUCATION 5
        [Display(Name = "School name")]
        public string Education5School { get; set; }

        [Display(Name = "Education-5 name")]
        public string Education5Name { get; set; }

        [Display(Name = "Education period")]
        public string Education5Period { get; set; }

        //EDUCATION 6
        [Display(Name = "School name")]
        public string Education6School { get; set; }

        [Display(Name = "Education-6 name")]
        public string Education6Name { get; set; }

        [Display(Name = "Education period")]
        public string Education6Period { get; set; }


    }
}