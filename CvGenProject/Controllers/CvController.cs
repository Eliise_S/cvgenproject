﻿using CvGenProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CvGenProject.Controllers
{
    public class CvController : Controller
    {
        TemplateContext db = new TemplateContext();

        // GET: Cv
        public ActionResult ShowCV(string id)
        {

            if (id != null)
            {
                List<TemplateForm> cvs = db.TemplateForms.ToList();

                TemplateForm item = cvs.First(i => i.CvUrl == id);

                ///List<string> skills = item.Technical.Split(',').ToList<string>();
                //skills.Reverse();

                //var SkillsFullLoops = skills.Count() / 3;
                //var SkillsReminder = skills.Count() % 3;

                //ViewBag.skillSet = skills;
                //ViewBag.fullLoops = SkillsFullLoops;
                //ViewBag.reminder = SkillsReminder;

                return View("ShowCV"+ item.TemplateId, item);
                
                
            }
            else
            {
                return View("NotFound");
            }
        }

        public ActionResult Cabinet()
        {
            var userName = User.Identity.Name;
            ViewBag.userName = userName;

            List<TemplateForm> cvs = db.TemplateForms.ToList();

            IEnumerable<TemplateForm> SelectedCvs = cvs.Where(i => i.UserId == userName);

            if (SelectedCvs != null)
            {
                return View(SelectedCvs);
            }
            else
            {
                return View("NotFound");
            }
        }

    }
}