﻿using CvGenProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CvGenProject.Controllers
{
    public class TemplatesController : Controller
    {
        TemplateContext db = new TemplateContext();

        public ActionResult Index()
        {
            return View(db.Templates);
        }

        public ActionResult Preview(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Template template = db.Templates.Find(id);
            if (template != null)
            {
                return View(template);
            }
            return HttpNotFound();
        }
        [HttpGet]
        public ActionResult Create(int id,string userId)
        {
            DateTime now = DateTime.Now;
            ulong generatedNumber = Hash(now);

            ViewBag.RandomNumber = generatedNumber;
            ViewBag.templateId = id;
            ViewBag.userId = userId;

            return View();
            
        }

        [HttpPost]
        public ActionResult Create(TemplateForm form)
        {
            string CvUrl = form.CvUrl;

            db.TemplateForms.Add(form);
            db.SaveChanges();

            
            return RedirectToAction("Success", new { url = CvUrl });

        }

        public ActionResult Success(string url)
        {
            ViewBag.CvUrl = url;
            return View("FormCreated");
        }

        public ActionResult Test()
        {
            DateTime now = DateTime.Now;
            ulong xxx = Hash(now);
            string stringFromUlong = xxx.ToString();
            ViewBag.zzz = stringFromUlong;

            return View();
        }

        public ActionResult CV(string id)
        {
            List<TemplateForm> cvs = db.TemplateForms.ToList();

            TemplateForm item = cvs.First(i => i.CvUrl == id);

            if (item != null)
            {
                return View(item);
            }
            else
            {
                return View("NotFound");
            }
            
        }

        private static ulong Hash(DateTime when)
        {
            ulong kind = (ulong)(int)when.Kind;
            return (kind << 62) | (ulong)when.Ticks;
        }


        

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}